<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use App\Http\Controllers\TestContrloller;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/test1', function () {
    return view('test');
});

Route::get('/test2', function () {
    return view('subview.test2');
});

//Route::get('/route1', function () {
//    $arry = ["name"=>"Nika", "Age"=>67];
//    return $arry;
//});


Route::get('/test3', [\App\Http\Controllers\TestContrloller::class, 'test1']);

Route::get('/test5', [\App\Http\Controllers\TestContrloller::class, 'test2']);

Route::any('/test6', [\App\Http\Controllers\TestContrloller::class, 'test2']);

//Route::get('/test5', 'TestContrloller@test2');

Route::redirect("/route12", "/route1");

Route::any('/route1', [\App\Http\Controllers\Test1Contrloller::class, 'route1'])->name("route1");

Route::get('/route2/{id}/test1/{cat}/cat2', function ($id=98, $catt=87) {
    return 'Id - > '.$id.' Cat - > '.$catt.' Cat2';
});

Route::get('/route3/{name}', function ($name) {
    //
    return $name;
})->where('name', '[A-Za-z]+');

Route::get('/route4/{id}', function ($id) {
    return $id;
});

Route::get('/route5', function () {
    return "Route5";
})->name("route5");


Route::prefix('admin')->group(function () {
    Route::get('/route1', function () {
       return "Admin Route 1";
    });
    Route::get('/route2', function () {
        return "Admin Route 2";
    });
});

Route::prefix('profile')->group(function () {
    Route::get('/route1', function () {
        return "Admin Route 1";
    });
    Route::get('/route2', function () {
        return "Admin Route 2";
    });
});

$routes = ["admin", "profile", "Products"];

for ($i=0; $i<10; $i++){
    Route::prefix('profile'.$i)->group(function () {
        Route::get('/route1', function () {
            return "Admin Route 1";
        });
        Route::get('/route2', function () {
            return "Admin Route 2";
        });
    });
}

Route::fallback(function () {
   return "Errorr";
});
